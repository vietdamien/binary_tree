/* *****************************************************************************
 * Project name: Binary_Tree
 * File name   : util
 * Author      : Damien Nguyen
 * Date        : Saturday, April 11 2020
 * ****************************************************************************/

#ifndef _BINARY_TREE_UTIL_H_
#define _BINARY_TREE_UTIL_H_

#include <stdio.h>

// ========================== CONSTANTS AND MACROS ========================== //
#define CHAR_EOL '\n'
#define CHAR_EOW '\0'

#define EMPTY_STR ""

#define FILE_READ  "r"
#define FILE_WRITE "w"

#define WORD_LEN 1024

// =============================== STRUCTURES =============================== //
typedef enum {
    false, true
} bool_t;

// ============================== DECLARATIONS ============================== //
void *alloc(size_t size);

#endif // _BINARY_TREE_UTIL_H_
