/* *****************************************************************************
 * Project name: Binary_Tree
 * File name   : util
 * Author      : Damien Nguyen
 * Date        : Saturday, April 11 2020
 * ****************************************************************************/

#include <errno.h>
#include <stdlib.h>

#include "util.h"

void *alloc(size_t size) {
    void *data = NULL;

    if (!(data = malloc(size))) {
        perror("alloc");
        exit(errno);
    }

    return data;
}
